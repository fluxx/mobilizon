/* eslint-disable @typescript-eslint/camelcase */

import ar from "./ar.json";
import be from "./be.json";
import ca from "./ca.json";
import cs from "./cs.json";
import de from "./de.json";
import en_US from "./en_US.json";
import es from "./es.json";
import fi from "./fi.json";
import fr_FR from "./fr_FR.json";
import it from "./it.json";
import ja from "./ja.json";
import nl from "./nl.json";
import oc from "./oc.json";
import pl from "./pl.json";
import pt from "./pt.json";
import pt_BR from "./pt_BR.json";
import ru from "./ru.json";
import sv from "./sv.json";

export default {
  ar,
  be,
  ca,
  cs,
  de,
  en: en_US,
  en_US,
  es,
  fi,
  fr: fr_FR,
  fr_FR,
  it,
  ja,
  nl,
  oc,
  pl,
  pt,
  pt_BR,
  ru,
  sv,
};
